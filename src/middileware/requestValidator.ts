/********************Function to check weather proper request is coming or not******************************************/
import {MESSAGE} from '../constants'
const checkRequest = (array, obj) => {
    for(const x of array){
        if(!obj[x]) return Promise.reject({message:`${x} ${MESSAGE.MISSING_KEY}`})
    }
    return true;
}

export const checkValidObkjects = (obj) => {
    const newObj: any = {};
    for (const [key, value] of Object.entries(obj)) {
        if (obj[key] != 'null' && obj[key] != 'undefined' && obj[key] != '')
            newObj[key] = value;
    }
    return Promise.resolve(newObj);
};

const vaidateKeys = (COobj,REQobj) => {
    let n = 0,storedKey = '';
    for(const [key, value] of Object.entries(REQobj)){
        if(n) return Promise.reject({message:`${REQobj[storedKey]} ${MESSAGE.MISSING_KEY}`})
        for(const [key, value] of Object.entries(COobj)){
            n = 0;
            if (COobj[key] == REQobj[key]){
                n = 1;
                storedKey = key;
            }
        }
    }
    return true;
}

export  {
    vaidateKeys,
    checkRequest
}
