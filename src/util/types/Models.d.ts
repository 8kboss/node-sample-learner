import { Model } from 'sequelize';

export interface IUsersModel extends Model {
    id?: number;
    email:string;
    password:string;
    status:boolean;
}
export interface IProductsModel extends Model {
    id?: number;
    name:string;
    size:string;
    price:number;
    category?:string;
    isDeleted?:boolean;
    deletedAt?:Date;
    status:boolean;
}