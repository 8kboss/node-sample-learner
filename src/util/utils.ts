/**************************Module Imports **************************************************/
import crypto from 'crypto';
import jsonWebToken from 'jsonwebtoken';
import * as fs from 'fs';

const http = require('http');
const uuid = require('node-uuid');
// const base64Img = require('base64-img');


/////////   generate folder if not
export const checkFolder = (path) => {
    if (!fs.existsSync(path)) {
        // if not then create
        fs.mkdirSync(path);
        return true;
    }
    return true;
};

// //////////   check validObjects 

export const checkValidObkjects = (obj) => {
    const newObj: any = {};
    for (const [key, value] of Object.entries(obj)) {
        if (obj[key] != 'null' && obj[key] != 'undefined' && obj[key] != '')
            newObj[key] = value;
    }
    return Promise.resolve(newObj);
};

checkFolder('./upload');
