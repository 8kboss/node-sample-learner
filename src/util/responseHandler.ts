import { MESSAGE, HTTP } from '../constants'
const responseHandler = (res, status, message, data) => {
    if(status === 200){
        return send(res,status, message, data)
    }else{
        return send(res,status, `Something went wrong!!!`)
    }
}


const errorHandler = (err, res) => {

    // email
    // console.log('err in errorHandler:>> ', err?.errors[0]?.ValidationErrorItem?.message);
    if (err && err.original && (err.original.sqlMessage).includes("'NaN'"))
        err.status = HTTP.BAD_REQUEST, err.message = MESSAGE.INVALID_PASSWORD
    
    if (!err.status) {
        err.status = 400;
    }
    
    if(err && err?.errors && err?.errors.length)
    err.message = err?.errors[0]?.message
    send(res,err.status,  err.message)
}


function send(res, status, message, data={}) {
    res.send({status: status,message: message,data: data})
};



export {
    responseHandler,errorHandler
}


