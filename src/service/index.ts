

import * as ProductsService from './ProductsService'
import * as UsersService from './UsersService'

export {
    ProductsService,
    UsersService
};