/*
 * Business logics
 *      or 
 * Calculations with reqest data
 * prepare response DTO(data transfer object)
 */
;
const uuid = require('node-uuid');
import {Products} from '../domain'
import {IProductsModel} from '../util/types/Models'

export async function add(params) {
    try {
      const product:IProductsModel = {
        name: params.name,
        size: params.size,
        category: params.category,
        price:params.price,
        status: params.status,
      }
      return await Products.create(product);
    } catch (error) {
      console.log("error :>> ", error);
      throw error;
    }
  }


export async function list(params) {
  try {
  
    return await Products.findAll({where:{isDeleted:false}});
  } catch (error) {
    console.log("error :>> ", error);
    throw error;
  }
}
  



export async function get(id:number) {
  try {
  
    return await Products.findOne({where:{id:id}});
  } catch (error) {
    console.log("error :>> ", error);
    throw error;
  }
}
  


export async function update(params:any,id:number) {
  try {
    const product:IProductsModel = {
      name: params.name,
      size: params.size,
      category: params.category,
      price:params.price,
      status: params.status,
    }
    return await Products.update(product,{where:{id:id}});
  } catch (error) {
    console.log("error :>> ", error);
    throw error;
  }
}



export async function remove(id:number) {
  try {
  
    return await Products.destroy({where:{id:id}});
  } catch (error) {
    console.log("error :>> ", error);
    throw error;
  }
}



