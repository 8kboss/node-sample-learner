const MESSAGE = {
    LOGIN_SUCCESSFUL: "Login Successful.",
    VERIFICATION_SUCCESSFUL: "Verifiation Successful.",
    UPDATE_SUCCESSFUL: "Update Successful.",
    SAVE_SUCCESSFUL: "Save Successful.",
    DELETE_SUCCESSFUL: "Delete Successful.",
    SUCCESS: "SUCCESS",
    REGISTRATION_SUCCESSFUL: "Registration Successful.",
    INVALID_PASSWORD:"Invalid Password",
    DUBLICATE_KEY: "{subject} Already Exists " ,
    MISSING_KEY: "key is missing"   
}



const HTTP = {
    SUCCESS: 200,
    CREATED: 201,
    ALREADY_EXISTS:208,
    BAD_REQUEST:400,
    ERROR:404
}

export {
   MESSAGE,
   HTTP

}