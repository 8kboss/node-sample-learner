import Sequelize from "sequelize";
const uuid = require("node-uuid");
import { mysql_connection } from "../config/mysql";
const Products = mysql_connection.define(
  "products",
  {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    name: { type: Sequelize.STRING, allowNull: false, unique: "product" },
    size: { type: Sequelize.STRING, allowNull: false },
    price: { type: Sequelize.INTEGER, allowNull: false },
    category: { type: Sequelize.STRING, allowNull: true },
    isDeleted: { type: Sequelize.BOOLEAN, allowNull: false, defaultValue:false },
    deletedAt: { type: Sequelize.BOOLEAN, allowNull: true },
  },
  { freezeTableName: true, timestamps: true }
);
// product.sync()
export { Products };
