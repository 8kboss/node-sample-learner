import Sequelize from "sequelize";
const uuid = require("node-uuid");
import { mysql_connection } from "../config/mysql";
const Users = mysql_connection.define(
  "users",
  {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false,unique: "email" 
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false,
      },

    status: { type: Sequelize.BOOLEAN, allowNull: true },
  },
  { freezeTableName: true, timestamps: true }
);

export { Users };
