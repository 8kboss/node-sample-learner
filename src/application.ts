require('dotenv').config()
import express from 'express';
import bodyParser from 'body-parser';
import chalk from 'chalk';
import cors from 'cors';
import morgan from 'morgan';

import { mysql_connection } from './config';
import {
   UsersController,
   ProductsController
} from './controller';

const app = express();
app.use(
    bodyParser.json({
        limit: '50mb',
        extended: false,
        type: 'application/json'
    })
);

app.use(
    bodyParser.urlencoded({
        limit: '200mb',
        extended: true,
        parameterLimit: 50000
    })
);
app.use(logErrors)
app.use(clientErrorHandler)
app.use(errorHandler)
function logErrors(err, req, res, next) {
    console.error(err.stack)
    next(err)
}
function clientErrorHandler(err, req, res, next) {
    if (req.xhr) {
        res.status(500).send({ error: 'Something wrong..!!!.Please try after some time.' })
    } else {
        next(err)
    }
}
function errorHandler(err, req, res, next) {
    res.status(500)
    res.render('error', { error: err })
}


app.use(cors());
app.use(morgan('common'))


app.get('/api/v1/health', (_, res) => res.status(200).json({ message: 'Greetings from Aie!' }));

import {
   Users, Products
} from './domain/index'

(async function () {
    // await Users.sync({alter:true});
    await Products.sync({alter:true});
})();

mysql_connection
app.use('/api/v1/products', ProductsController)
app.use('/api/v1/users', UsersController)

export { app }
