require('dotenv').config()
import { Router } from 'express'
import {errorHandler, responseHandler} from '../util/responseHandler'
import { MESSAGE,HTTP } from '../constants'
import { ProductsService } from '../service'



const route = Router();


route.post('/', async function (req, res) {
    try {
        responseHandler(res, HTTP.SUCCESS, MESSAGE.REGISTRATION_SUCCESSFUL, await ProductsService.add(req.body));
    } catch (err) { errorHandler(err, res)}
}).get('/', async function (req, res) {
    try {
        responseHandler(res, HTTP.SUCCESS, MESSAGE.SUCCESS, await ProductsService.list(req.body));
    } catch (err) { errorHandler(err, res)}
}).get('/:id', async function (req, res) {
    try {
        responseHandler(res, HTTP.SUCCESS, MESSAGE.SUCCESS, await ProductsService.get(req.params.id));
    } catch (err) { errorHandler(err, res)}
}).put('/:id', async function (req, res) {
    try {
        responseHandler(res, HTTP.SUCCESS, MESSAGE.UPDATE_SUCCESSFUL, await ProductsService.update(req.body, req.params.id));
    } catch (err) { errorHandler(err, res)}
}).delete('/:id', async function (req, res) {
    try {
        responseHandler(res, HTTP.SUCCESS, MESSAGE.DELETE_SUCCESSFUL, await ProductsService.remove(req.params.id));
    } catch (err) { errorHandler(err, res)}
})

export default route;