require('dotenv').config()
import { Router } from 'express'
import { UsersService } from '../service'
import {errorHandler, responseHandler} from '../util/responseHandler'
import { MESSAGE,HTTP } from '../constants'
const route = Router();
/**
 * save user
 */
route.post('/register', async function (req, res) {
    try {
        responseHandler(res, HTTP.SUCCESS, MESSAGE.REGISTRATION_SUCCESSFUL, await UsersService.register(req.body));
    } catch (err) { errorHandler(err, res)}
}).post('/login', async function (req, res) {
    try {
        responseHandler(res, HTTP.SUCCESS, MESSAGE.LOGIN_SUCCESSFUL, await UsersService.login(req.body));
    } catch (err) { errorHandler(err, res)}
}).post('/forget', async function (req, res) {
    try {
        responseHandler(res, HTTP.SUCCESS, MESSAGE.REGISTRATION_SUCCESSFUL, await UsersService.forget(req.body));
    } catch (err) { errorHandler(err, res)}
})


export default route;