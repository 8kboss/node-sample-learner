require('dotenv').config()
import Sequelize from 'sequelize';

const { MYSQL_DATABASE, MYSQL_DB_USER, MYSQL_DB_PASSWORD, MYSQL_HOST, } = process.env;

if (!MYSQL_HOST) {
    console.log(`[mysql] $MYSQL_HOST value: ${MYSQL_HOST}`);
    process.exit(1);
}

const [MYSQL_HOST_IP, MYSQL_HOST_PORT] = MYSQL_HOST.split(':');

const sequelize = new Sequelize(MYSQL_DATABASE, MYSQL_DB_USER, MYSQL_DB_PASSWORD, {
    host: MYSQL_HOST_IP,
    dialect: 'mysql',
    operatorsAliases: false,
    // logging:false,

    pool: {
        max: 10,
        min: 10,
        acquire: 30000,
        idle: 10000,
      },
});


sequelize.authenticate()
    .then(() => {
        console.log('[sequelize] Connection has been established successfully.');
    })
    .catch(err => {
        console.error('[sequelize] Unable to connect to the database:', err);
    });
const mysql_connection = sequelize;
export { mysql_connection };
export default sequelize;
